module.exports = {
    url: 'http://localhost',
    port: 3000,
    originUrl: 'http://localhost:3000',
    mongoUrl: 'mongodb://localhost:27017/staart',
    sharedSecret: '!123654#',
    sessionSecret: '!123654#',
    oothPath: '/auth',
    mailgun: {
        apiKey: "XXX",
        domain: "XXX"
    },
    mail: {
        from: "info@example.com"
    },
    facebook: {
        clientID: 'XXX',
        clientSecret: 'XXX',
    },
    google: {
        clientID: 'XXX',
        clientSecret: 'XXX',
    },
}

import { Component, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService} from '../service/index';

@Component({
    selector: 'app-header',
    template: require('../views/app-header.component.html'),
})
export class HeaderComponent {

    sideNavVisible:boolean = false;
    loggedInUserName: string;

    constructor(
        private router: Router,
        private authService: AuthService,
        private el: ElementRef) {
            let loggedInInfo = this.authService.loggedInInfo;
            if (loggedInInfo) this.loggedInUserName = loggedInInfo.username;
        }

    @HostListener('click', ['$event']) onClick(event:any) {
        if (event.target.id == 'sidenav-overlay') this.sideNavVisible = false;  
    }
}
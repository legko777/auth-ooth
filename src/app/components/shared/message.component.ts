import { Component, OnInit } from '@angular/core';

import { MessageService } from '../../service/index';

@Component({
    selector: 'message',
    template: require('../../views/shared/message.component.html')
})

export class MessageComponent {
    message: any[];

    constructor(private msgService: MessageService) { }

    ngOnInit() {
        this.msgService.getMessage().subscribe(message => { 
            this.message = message; 
            let self = this;
            setTimeout(function() {
                $('#toast-container').fadeOut('xslow', function() {
                    $('#toast-container').remove();
                    self.message = undefined;
                });
            }, 5000);
        });
    }
}
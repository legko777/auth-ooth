import { Component, HostListener, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit, ElementRef } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthService, MessageService } from '../service/index';
import configs from '../configs'; 

@Component({
    selector: 'login',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: require('../views/login.component.html')
})

export class LoginComponent {

    private returnUrl: string;

    private loginForm: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private msgService: MessageService,
        private cd: ChangeDetectorRef,
        private el: ElementRef) {
        // reset login status
        this.authService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

        this.loginForm = new FormGroup({
            userName: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        });
    }

    private login() {

        $('#spinner-container').show("slow");
        if (this.loginForm.userName && this.loginForm.password) {
            const password = this.loginForm.password;
            this.authService.login(this.loginForm.userName, password).subscribe(data => {
                $('#spinner-container').hide("slow");
                this.router.navigate(['home']);
            },
                error => {
                    $('#spinner-container').hide("slow");
                    this.msgService.error(error.json().message);
                });
        } else {
            $('#spinner-container').hide("slow");
            this.msgService.error('Username or password is incorrect');
        }
    }
}
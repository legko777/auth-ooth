import { Component, AfterViewInit } from '@angular/core';
import configs from '../../configs';
import { AuthService } from '../../service/index';

@Component({
    selector: 'home',
    templateUrl: '../../views/home/home.component.html',
})

export class HomeComponent{

    loggedInUser: any;
    
    private todoStatus: any = {
        completed: 'completed',
        notCompleted: 'notCompleted'
    };

    constructor(
        private authService: AuthService){
            this.loggedInUser = this.authService.loggedInInfo;
    }

 }
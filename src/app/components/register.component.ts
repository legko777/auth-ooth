import { Component, HostListener, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit, ElementRef } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthService, MessageService } from '../service/index';
import configs from '../configs'; 

@Component({
    selector: 'signup',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: require('../views/register.component.html')
})

export class RegisterComponent {

    private registerForm: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private msgService: MessageService,
        private cd: ChangeDetectorRef,
        private el: ElementRef) {
        // reset login status
        this.authService.logout();
        // get return url from route parameters or default to '/'
        this.registerForm = new FormGroup({
            fullName: new FormControl('', Validators.required),
            email: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required)
        });
    }

    private login() {

        $('#spinner-container').show("slow");
        if (this.registerForm.email && this.registerForm.password) {
            const password = this.registerForm.password;
            this.authService.register(this.registerForm.fullName,this.registerForm.email, password).subscribe(data => {
                $('#spinner-container').hide("slow");
                let response = data.json();
                if (response && response.message) {
                    this.router.navigate(['login']);
                    this.msgService.success(response.message);
                }
                
            },
                error => {
                    $('#spinner-container').hide("slow");
                    this.msgService.error(error.json().message);
                });
        } else {
            $('#spinner-container').hide("slow");
            this.msgService.error('All fields are required');
        }
    }
}
import { Component, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService} from '../service/index';


@Component({
    selector: 'logout',
    template: ''
})

export class LogoutComponent {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService) {
            // reset login status
            this.authService.logout();
            location.href = '/#/login';
        }
}
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { JwtHelper } from 'angular2-jwt';

import { routing } from './app.routes';
import { 
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    MessageComponent,
    LogoutComponent,
    RegisterComponent } from './components/index';

import {  
    StorageService,
    AuthGuardService,
    AuthService,
    MessageService } from './service/index';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        routing
    ],
    providers: [
        StorageService,
        JwtHelper,
        AuthGuardService,
        AuthService,
        MessageService
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        LoginComponent,
        MessageComponent,
        LogoutComponent,
        RegisterComponent
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
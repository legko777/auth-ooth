import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { JwtHelper } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import configs from '../configs';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class AuthService {

    public getActiveRoute() {
        const activeRoute = location.href;
        if (activeRoute.indexOf('login') != -1) return '';
        else return activeRoute;
    }

    constructor(
        private jwtHelper: JwtHelper,
        private http: Http,
        private route: ActivatedRoute) { }

    public isAuthenticated(): boolean {
        const authorizationData = localStorage.getItem('authorizationData');
        if (!authorizationData) return false;
        return true;
        /*let token = JSON.parse(authorizationData).token;
        if (!token) {
            this.logout();
            return false;
        }
        return this.jwtHelper.isTokenExpired(token);*/
    }

    get loggedInInfo(): any {
        const authorizationData = JSON.parse(localStorage.getItem('authorizationData'));
        return authorizationData;
    }
    login(username: string, password: string) {
        const body = JSON.stringify({ username: username, password: password });
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(configs.api.root + '/auth/local/login', body,{
            headers: headers
          })
            .map((data: Response) => {
                let response = data.json();
                if (response) {
                    // store login response local storage to keep user logged in between page refreshes
                    localStorage.setItem('authorizationData', JSON.stringify(response));
                }
            });
    }

    logout() {
        let loggedInInfo= this.loggedInInfo;
        if (!loggedInInfo) return;
        
        const sessionId = this.loggedInInfo.sessionId; 
        return this.http.post(configs.api.root + '/auth/logout', null)
            .map((data: Response) => {
                debugger;
                let response = data.json();
                if (response == 'success') {
                    // remove login data from local storage to log user out
                    localStorage.removeItem('authorizationData');
                }
            }).subscribe( data => {
                localStorage.removeItem('authorizationData');
            }, err => {
                var errRes = err.json();
                if (errRes.message == "Not logged in"){
                    localStorage.removeItem('authorizationData');
                }
            });
        
    }

    register(fullName: string, email: string, password: string) {
        const body = JSON.stringify({ fullName: fullName, email: email, password: password });
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(configs.api.root + '/auth/local/register', body,{
            headers: headers
          });
    }
}
const configs = {
    api: {
        root: 'http://localhost:3000'
    },
    routes: {
        login: '/#/login',
        signup: '/#/signup',
        home: '/#/'
    }, 
    storageKey: {
        authorizationData: 'authorizationData'
    }
};

export default configs;
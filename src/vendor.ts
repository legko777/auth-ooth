// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';
// RxJS
import 'rxjs';
// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...



import '../node_modules/font-awesome/css/font-awesome.min.css';
import '../node_modules/mdbootstrap/css/bootstrap.min.css';
import '../node_modules/mdbootstrap/css/mdb.min.css';

import './assets/css/spinner.css';
import './assets/css/message.css';
import './assets/css/styles.css';
import './assets/css/side-nav.css';


//import 'mdbootstrap/js/jquery-3.2.1.min.js';
//import 'mdbootstrap/js/popper.min.js';
//import 'mdbootstrap/js/bootstrap.min.js';
//import 'mdbootstrap/js/mdb.min.js';